﻿# Host: localhost  (Version 5.5.5-10.4.11-MariaDB)
# Date: 2021-04-24 20:06:01
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "status"
#

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_description` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "status"
#

INSERT INTO `status` VALUES (1,'active'),(2,'inactive');

#
# Structure for table "city"
#

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(60) NOT NULL DEFAULT '',
  `city_status` int(6) NOT NULL DEFAULT 1,
  PRIMARY KEY (`city_id`),
  KEY `city_status_to_status_ID` (`city_status`),
  CONSTRAINT `city_status_to_status_ID` FOREIGN KEY (`city_status`) REFERENCES `status` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

#
# Data for table "city"
#

INSERT INTO `city` VALUES (1,'Bogotá DC',1),(2,'Medellin',1),(33,'Tunja',1),(34,'Bucaramanga',1),(35,'Chia',1),(36,'Cali',1);

#
# Structure for table "type_vehicle"
#

DROP TABLE IF EXISTS `type_vehicle`;
CREATE TABLE `type_vehicle` (
  `type_vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_vehicle_description` varchar(30) NOT NULL DEFAULT '',
  `type_vehicle_status` int(3) NOT NULL DEFAULT 1,
  PRIMARY KEY (`type_vehicle_id`),
  KEY `type_vehicle_to_status` (`type_vehicle_status`),
  CONSTRAINT `type_vehicle_to_status` FOREIGN KEY (`type_vehicle_status`) REFERENCES `status` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "type_vehicle"
#

INSERT INTO `type_vehicle` VALUES (1,'Particular ',1),(2,'Publico',1);

#
# Structure for table "user_type"
#

DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_desciption` varchar(40) NOT NULL DEFAULT '',
  `user_type_status` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`user_type_id`),
  KEY `user_type_to_status` (`user_type_status`),
  CONSTRAINT `user_type_to_status` FOREIGN KEY (`user_type_status`) REFERENCES `status` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "user_type"
#

INSERT INTO `user_type` VALUES (1,'Conductor',1),(2,'Propietario',1);

#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `identity_card` bigint(13) NOT NULL DEFAULT 0,
  `first_name` varchar(25) NOT NULL DEFAULT '',
  `second_name` varchar(25) DEFAULT '',
  `last_name` varchar(25) NOT NULL DEFAULT '',
  `address` varchar(60) NOT NULL DEFAULT '',
  `phone_number` bigint(15) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `user_type` int(3) DEFAULT NULL,
  `user_status` int(6) NOT NULL DEFAULT 1,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `unique` (`identity_card`) COMMENT 'unique_key',
  KEY `users_city_to_city` (`city`),
  KEY `users_to_user_type` (`user_type`),
  KEY `users_to_satus` (`user_status`),
  CONSTRAINT `users_city_to_city` FOREIGN KEY (`city`) REFERENCES `city` (`city_id`),
  CONSTRAINT `users_to_satus` FOREIGN KEY (`user_status`) REFERENCES `status` (`status_id`),
  CONSTRAINT `users_to_user_type` FOREIGN KEY (`user_type`) REFERENCES `user_type` (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (1,803984750,'David','Andres','Cardenas Moras','Calle 10 # 13-22 sur',3114587823,1,1,1),(2,1023945860,'Carlos','Adolfo','Nieto Hernández','Calle 12 # 45-82',3114367832,1,2,1),(3,895423341,'Ramiro','Javier','Montaña','Av 1 # 45-32',3124567899,1,1,1),(4,12345678,'Daniel','Alberto','Rojas Pinilla','calle 12 #23-12',314566677,2,2,1),(10,897856232,'Martha',NULL,'Corredor','Calle 10 # 19-23',31231231232,2,2,1),(11,2141242313,'Paola','Daniela','Ardila ','Carrera 22 # 17-32',3114589233,36,2,1);

#
# Structure for table "vehicle"
#

DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE `vehicle` (
  `vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_license_plate` varchar(20) NOT NULL DEFAULT '',
  `vehicle_color` varchar(15) NOT NULL DEFAULT '',
  `vehicle_brand` varchar(30) NOT NULL DEFAULT '',
  `type_vehicle` int(11) DEFAULT NULL,
  `user_driver` int(11) NOT NULL DEFAULT 0,
  `user_owner` int(11) NOT NULL DEFAULT 0,
  `vehicle_status` int(3) NOT NULL DEFAULT 1,
  PRIMARY KEY (`vehicle_id`),
  KEY `vehicle_to_type_vehicle` (`type_vehicle`),
  KEY `vehicle_to_users` (`user_driver`),
  KEY `vehicle_to_userId` (`user_owner`),
  KEY `vehicle_to_status` (`vehicle_status`),
  CONSTRAINT `vehicle_to_status` FOREIGN KEY (`vehicle_status`) REFERENCES `status` (`status_id`),
  CONSTRAINT `vehicle_to_type_vehicle` FOREIGN KEY (`type_vehicle`) REFERENCES `type_vehicle` (`type_vehicle_id`),
  CONSTRAINT `vehicle_to_userId` FOREIGN KEY (`user_owner`) REFERENCES `users` (`user_id`),
  CONSTRAINT `vehicle_to_users` FOREIGN KEY (`user_driver`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "vehicle"
#

INSERT INTO `vehicle` VALUES (1,'MOL444','Blanco','Ford',1,1,3,1),(2,'BTO201','Rojo','JAC',1,2,3,1),(3,'MOK312','Rojo','Ford',1,4,3,1),(5,'UHV789','Negro','Ferrari',2,1,2,1),(6,'WER213','Rosa','JAC',1,2,10,1),(7,'QRE234','Negro','BMW',1,2,11,1);
