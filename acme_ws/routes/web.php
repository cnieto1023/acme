<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//city
$router->get('ws/city/list', ['as' => 'User', 'uses' => 'CityController@list']);

//status
$router->get('ws/status/list', ['as' => 'User', 'uses' => 'StatusController@list']);

//TypeVehicle
$router->get('ws/TypeVehicle/list', ['as' => 'User', 'uses' => 'TypeVehiculeController@list']);

//UserType
$router->get('ws/UserType/list', ['as' => 'User', 'uses' => 'UserTypeController@list']);

//User
$router->get('ws/Users/list', ['as' => 'User', 'uses' => 'UsersController@list']);
$router->get('ws/Users/searchUser/{id}', ['as' => 'User', 'uses' => 'UsersController@searchUser']);
$router->post('ws/Users/insert', ['as' => 'User', 'uses' => 'UsersController@insert']);
$router->post('ws/Users/update', ['as' => 'User', 'uses' => 'UsersController@update']);

//Vehicle
$router->get('ws/Vehicle/list', ['as' => 'User', 'uses' => 'VehicleController@list']);
$router->get('ws/Vehicle/SerchVehicle/{id}', ['as' => 'User', 'uses' => 'VehicleController@searchVehicle']);
$router->post('ws/Vehicle/insert', ['as' => 'User', 'uses' => 'VehicleController@insert']);
$router->post('ws/Vehicle/update', ['as' => 'User', 'uses' => 'VehicleController@update']);


