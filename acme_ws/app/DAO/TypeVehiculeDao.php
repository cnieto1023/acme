<?php

namespace App\DAO;

use App\Models\TypeVehicleModel;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class TypeVehiculeDao
{

    function Tolist()
    {
        try {
            $data = TypeVehicleModel::where('type_vehicle_status', 1)->with('type_vehicle_status')->get();
            return $data;
        } catch (QueryException $e) {
            return $e;
        }
    }

}
