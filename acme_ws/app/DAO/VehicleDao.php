<?php

namespace App\DAO;

use App\Models\VehicleModel;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class VehicleDao
{

    function Tolist()
    {
        try {
            $data = VehicleModel::with('type_vehicle')
                                ->with('user_driver')
                                ->with('user_owner')
                                ->with('vehicle_status')
                                ->orderBy('vehicle_license_plate', 'ASC')
                                ->get();
            return $data;
        } catch (QueryException $e) {
            return $e;
        }
    }

    public function SerchBeforeCreate($params)
    {
        $response = VehicleModel::where('vehicle_license_plate',$params)->get();

        if(count($response) > 0)
        {
            return $response[0];
        }else{
            return false;
        }

    }

    public function SerchVehicle($params)
    {
        $response = VehicleModel::where('vehicle_id',$params)->get();

        return $response;

    }

    public function insert($request)
    {
        $response = $this->SerchBeforeCreate($request['vehicle_license_plate']);

        if($response != false)
        {
            return 'Vehicle '.$response['vehicle_license_plate'].' already exits, please verify information.';
        }else{
            try {
                $data = array(
                    'vehicle_license_plate'=> $request['vehicle_license_plate'],
                    'vehicle_color'=> $request['vehicle_color'],
                    'vehicle_brand'=> $request['vehicle_brand'],
                    'type_vehicle'=> $request['type_vehicle'],
                    'user_driver'=> $request['user_driver'],
                    'user_owner'=> $request['user_owner'],
                    'vehicle_status'=> 1
                );

                $response = VehicleModel::insert($data);
                return response() -> json($response, 201);

            } catch (QueryException $e) {
                return response() -> json($e, 400);
            }
        }
    }

    public function update($request)
    {
        $vehicle_id = $request['vehicle_id'];
        try {
            $data = array(
                'vehicle_license_plate'=> $request['vehicle_license_plate'],
                'vehicle_color'=> $request['vehicle_color'],
                'vehicle_brand'=> $request['vehicle_brand'],
                'type_vehicle'=> $request['type_vehicle'],
                'user_driver'=> $request['user_driver'],
                'user_owner'=> $request['user_owner'],
                'vehicle_status'=> $request['vehicle_status']
            );

        $response = VehicleModel::findOrFail($vehicle_id) -> update($data);
        return response() -> json($response, 200);

        } catch (QueryException $e) {
            return response() -> json($e, 400);
        }
    }

}
