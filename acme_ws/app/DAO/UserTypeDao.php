<?php

namespace App\DAO;

use App\Models\UserTypeModel;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class UserTypeDao
{

    function Tolist()
    {
        try {
            $data = UserTypeModel::where('user_type_status', 1)->with('user_type_status')->get();
            return $data;
        } catch (QueryException $e) {
            return $e;
        }
    }

}
