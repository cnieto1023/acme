<?php

namespace App\DAO;

use App\Models\StatusModel;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class StatusDao
{

    function Tolist()
    {
        try {
            $data = StatusModel::all();
            return $data;
        } catch (QueryException $e) {
            return $e;
        }
    }

}
