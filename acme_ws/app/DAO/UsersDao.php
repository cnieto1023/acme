<?php

namespace App\DAO;

use App\Models\UsersModel;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class UsersDao
{

    function Tolist()
    {
        try {
            $data = UsersModel::with('city')
                                ->with('user_type')
                                ->with('user_status')
                                ->orderBy('first_name', 'ASC')
                                ->get();
            return $data;
        } catch (QueryException $e) {
            return $e;
        }
    }

    public function SerchBeforeCreateUser($params)
    {
        $response = UsersModel::where('identity_card',$params)->get();

        if(count($response) > 0)
        {
            return $response[0];
        }else{
            return false;
        }

    }

    public function SerchUser($params)
    {
        $response = UsersModel::where('user_id',$params)->get();

        return $response;

    }

    public function insertarNewUser($request)
    {
        $response = $this->SerchBeforeCreateUser($request['identity_card']);

        if($response != false)
        {
            return 'User '.$response['identity_card'].' already exits with this name '
                    .$response['first_name'].' '.$response['second_name'].' '.$response['last_name'].' please verify information.';
        }else{
            try {
                $data = array(
                    'identity_card'=> $request['identity_card'],
                    'first_name'=> $request['first_name'],
                    'second_name'=> $request['second_name'],
                    'last_name'=> $request['last_name'],
                    'address'=> $request['address'],
                    'phone_number'=> $request['phone_number'],
                    'city'=> $request['city'],
                    'user_type'=> $request['user_type'],
                    'user_status'=> 1
                );

                $response = UsersModel::insert($data);
                return response() -> json($response, 201);

            } catch (QueryException $e) {
                return response() -> json($e, 400);
            }
        }
    }

    public function update($request)
    {
        $user_id = $request['user_id'];
        try {
            $data = array(
                'identity_card'=> $request['identity_card'],
                'first_name'=> $request['first_name'],
                'second_name'=> $request['second_name'],
                'last_name'=> $request['last_name'],
                'address'=> $request['address'],
                'phone_number'=> $request['phone_number'],
                'city'=> $request['city'],
                'user_type'=> $request['user_type'],
                'user_status'=> $request['user_status']
            );

        $response = UsersModel::findOrFail($user_id) -> update($data);
        return response() -> json($response, 200);

        } catch (QueryException $e) {
            return response() -> json($e, 400);
        }
    }

}
