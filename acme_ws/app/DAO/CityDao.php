<?php

namespace App\DAO;

use App\Models\CityModel;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class CityDao
{

    function Tolist()
    {
        try {
            $data = CityModel::where('city_status', 1)->with('city_status')->orderBy('city_name', 'ASC')->get();
            return $data;
        } catch (QueryException $e) {
            return $e;
        }
    }

}
