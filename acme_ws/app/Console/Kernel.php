<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Http\Controllers\Sap13InterfaceMaterialMasterController;
use App\Http\Controllers\Sap13InterfaceVendorMasterController;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $Sap13InterfaceMaterialMasterController = new Sap13InterfaceMaterialMasterController();
            $response = $Sap13InterfaceMaterialMasterController->ToStart();
        })->dailyAt('07:00')->after(function () {
            $Sap13InterfaceVendorMasterController = new Sap13InterfaceVendorMasterController();
            $res = $Sap13InterfaceVendorMasterController->ToStart();
        });
    }
}
