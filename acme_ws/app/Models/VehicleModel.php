<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{
	//selected table
	protected $table = 'vehicle';

	//table fiels
    protected $fillable = [
        'vehicle_id',
        'vehicle_license_plate',
        'vehicle_color',
        'vehicle_brand',
        'type_vehicle',
        'user_driver',
        'user_owner',
        'vehicle_status'
    ];

    //primary key
    protected $primaryKey = 'vehicle_id';

    public $timestamps = false;

    //hidden elements
    protected $hidden = [
    ];

     //foreing keys
     public function type_vehicle()
     {
         return $this -> belongsTo(TypeVehicleModel::class, 'type_vehicle');
     }

     public function user_driver()
     {
         return $this -> belongsTo(UsersModel::class, 'user_driver')->with('city');
     }

     public function user_owner()
     {
         return $this -> belongsTo(UsersModel::class, 'user_owner')->with('city');
     }

     public function vehicle_status()
     {
         return $this -> belongsTo(StatusModel::class, 'vehicle_status');
     }

}

