<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTypeModel extends Model
{
	//selected table
	protected $table = 'user_type';

	//table fiels
    protected $fillable = [
        'user_type_id',
        'user_type_desciption',
        'user_type_status'
    ];

    //primary key
    protected $primaryKey = 'user_type_id';

    public $timestamps = false;

    //hidden elements
    protected $hidden = [
    ];

     //foreing keys
     public function user_type_status()
     {
         return $this -> belongsTo(StatusModel::class, 'user_type_status');
     }

}

