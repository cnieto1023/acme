<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusModel extends Model
{
	//selected table
	protected $table = 'status';

	//table fiels
    protected $fillable = [
        'status_id',
        'status_description'
    ];

    //primery key
    protected $primaryKey = 'status_id';

    public $timestamps = false;

    //hide elements
    protected $hidden = [
    ];

}

