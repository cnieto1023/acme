<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityModel extends Model
{
	//selected table
	protected $table = 'city';

	//table fiels
    protected $fillable = [
        'city_id',
        'city_name',
        'city_status'
    ];

    //primary key
    protected $primaryKey = 'city_id';

    public $timestamps = false;

    //hidden elements
    protected $hidden = [
    ];

     //foreing keys
     public function city_status()
     {
         return $this -> belongsTo(StatusModel::class, 'city_status');
     }

}

