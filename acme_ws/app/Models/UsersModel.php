<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersModel extends Model
{
	//selected table
	protected $table = 'users';

	//table fiels
    protected $fillable = [
        'user_id',
        'identity_card',
        'first_name',
        'second_name',
        'last_name',
        'address',
        'phone_number',
        'city',
        'user_type',
        'user_status'
    ];

    //primary key
    protected $primaryKey = 'user_id';

    public $timestamps = false;

    //hidden elements
    protected $hidden = [
    ];

     //foreing keys
     public function city()
     {
         return $this -> belongsTo(CityModel::class, 'city');
     }

     public function user_type()
     {
         return $this -> belongsTo(UserTypeModel::class, 'user_type');
     }

     public function user_status()
     {
         return $this -> belongsTo(StatusModel::class, 'user_status');
     }

}

