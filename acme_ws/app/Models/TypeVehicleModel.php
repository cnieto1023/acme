<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeVehicleModel extends Model
{
	//selected table
	protected $table = 'type_vehicle';

	//table fiels
    protected $fillable = [
        'type_vehicle_id',
        'type_vehicle_description',
        'type_vehicle_status'
    ];

    //primary key
    protected $primaryKey = 'type_vehicle_id';

    public $timestamps = false;

    //hide elements
    protected $hidden = [
    ];

     //foreing keys
     public function type_vehicle_status()
     {
         return $this -> belongsTo(StatusModel::class, 'type_vehicle_status');
     }

}

