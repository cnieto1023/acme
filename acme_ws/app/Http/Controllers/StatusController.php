<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\DAO\StatusDao;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application as App;


class StatusController extends BaseController
{

    public function list()
    {
        try {
                $dao = new StatusDao();
                $response = $dao->Tolist();

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

}
