<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\DAO\TypeVehiculeDao;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application as App;


class TypeVehiculeController extends BaseController
{

    public function list()
    {
        try {
                $dao = new TypeVehiculeDao();
                $response = $dao->Tolist();

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

}
