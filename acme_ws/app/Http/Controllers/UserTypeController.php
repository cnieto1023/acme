<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\DAO\UserTypeDao;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application as App;


class UserTypeController extends BaseController
{

    public function list()
    {
        try {
                $dao = new UserTypeDao();
                $response = $dao->Tolist();

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

}
