<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\DAO\UsersDao;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application as App;


class UsersController extends BaseController
{

    public function list()
    {
        try {
                $dao = new UsersDao();
                $response = $dao->Tolist();

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

    public function insert(request $request)
    {
        try {
                $dao = new UsersDao();
                $response = $dao->insertarNewUser($request);

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

    public function update(request $request)
    {
        try {
                $dao = new UsersDao();
                $response = $dao->update($request);

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

    public function searchUser($id)
    {
        try {
                $dao = new UsersDao();
                $response = $dao->SerchUser($id);

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

}
