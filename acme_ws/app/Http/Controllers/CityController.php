<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\DAO\CityDao;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application as App;


class CityController extends BaseController
{

    public function list()
    {
        try {
                $dao = new CityDao();
                $response = $dao->Tolist();

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

}
