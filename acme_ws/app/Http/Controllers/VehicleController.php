<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\DAO\VehicleDao;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application as App;


class VehicleController extends BaseController
{

    public function list()
    {
        try {
                $dao = new VehicleDao();
                $response = $dao->Tolist();

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

    public function insert(request $request)
    {
        try {
                $dao = new VehicleDao();
                $response = $dao->insert($request);

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

    public function update(request $request)
    {
        try {
                $dao = new VehicleDao();
                $response = $dao->update($request);

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

    public function searchVehicle($id)
    {
        try {
                $dao = new VehicleDao();
                $response = $dao->SerchVehicle($id);

                return $response;
            }
        catch (Exception $e) {
            return $e;
        }
    }

}
